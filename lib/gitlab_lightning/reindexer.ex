defmodule Explorer.Reindexer do
  alias Explorer.Repo
  import Ecto.Query

  @indexer Explorer.Indexer

  def reindex(indexer \\ @indexer) do
    syncs = Repo.all(Explorer.IssueSync)

    syncs
    |> Enum.each(fn sync ->
      :ok = indexer.index_sync(sync.gitlab_host, sync.project_path, sync.label)
    end)

    :ok
  end
end
