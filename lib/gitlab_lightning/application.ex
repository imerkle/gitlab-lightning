defmodule Explorer.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      supervisor(Explorer.Repo, []),
      supervisor(ExplorerWeb.Endpoint, []),
      worker(Explorer.ReindexScheduler, [])
    ]

    opts = [strategy: :one_for_one, name: Explorer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def config_change(changed, _new, removed) do
    ExplorerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
