defmodule ExplorerWeb.SearchController do
  use ExplorerWeb, :controller

  def show(conn, params) do
    query = params["q"]
    {:ok, issues} = Explorer.Searcher.search(query)

    render(conn, "show.html", query: query, issues: issues)
  end
end
