defmodule Explorer.IndexAndSearchTest do
  use Explorer.DataCase

  test "index and scan" do
    :ok =
      Explorer.Indexer.index_sync(
        "https://gitlab.com",
        "DylanGriffith/gitlab-lightning",
        "TestData"
      )

    {:ok, [issue]} = Explorer.Searcher.search("pop")

    assert issue.title == "Make it pop"
    assert issue.gitlab_created_at.day == 22
    assert issue.gitlab_updated_at.day == 23
  end
end
