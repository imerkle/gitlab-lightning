defmodule Explorer.FeatureCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      @tag :feature
      use Wallaby.DSL

      alias Explorer.Repo
      import Ecto
      import Ecto.Changeset
      import Ecto.Query

      import ExplorerWeb.Router.Helpers
    end
  end

  setup tags do
    {:ok, _} = Application.ensure_all_started(:wallaby)

    Application.put_env(:wallaby, :base_url, ExplorerWeb.Endpoint.url())

    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Explorer.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Explorer.Repo, {:shared, self()})
    end

    metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(Explorer.Repo, self())
    {:ok, session} = Wallaby.start_session(metadata: metadata)
    {:ok, session: session}
  end
end
